﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dlp.Connectors;
using CurvasDTO;


namespace CurveFeeder
{
    class Queries
    {

        public static List<AReceber> AReceber()
        {

            string dbWillConnectionString = Global.ConnectionStringBDWill;

            DatabaseConnector dbWill = new DatabaseConnector(dbWillConnectionString, 100000);

            string Query = @"SELECT   [DT_EXPCT_SETTLEMENT] AS ExpectedSettlementDate
                                     ,[NM_BANDEIRA] as Brand
                                     ,[CD_PRODUTO] as Produto
                                     ,[CD_TXTYPE] as TransactionType
                                     ,[VL_GROSS_AMOUNT] as Valor
                                     ,[VL_IC] as InterChange

                                 FROM [BD_FINANCEIRO].[dbo].[TBSTONEF_A_RECEBER]

                                 WHERE cast(DH_TIMESTAMP as date) = '2017-06-14'
                                 ORDER BY DT_EXPCT_SETTLEMENT ASC";

            List<AReceber> AReceber = dbWill.ExecuteReader<AReceber>(Query).ToList();

            dbWill.Close();

            return AReceber;

        }

        public static List<APagar> APagar()
        {

            string dbWillConnectionString = Global.ConnectionStringBDWill;

            DatabaseConnector dbWill = new DatabaseConnector(dbWillConnectionString, 100000);

            string Query = @" SELECT         DT_PAYMT AS Dia
                                         	,VL_A_PAGAR AS Valor

                             FROM BD_FINANCEIRO.dbo.TBSTONEF_A_PAGAR (NOLOCK)
                             WHERE cast(DH_TIMESTAMP as date) = '2017-06-14'
                             ORDER BY 1 ASC";

            List<APagar> APagar = dbWill.ExecuteReader<APagar>(Query).ToList();

            dbWill.Close();

            return APagar;

        }

        public static List<DadosCessao> DadosCessao()
        {
            string dbWillConnectionString = Global.ConnectionStringBDWill;

            DatabaseConnector dbWill = new DatabaseConnector(dbWillConnectionString, 100000);

            string Query = @"SELECT [NM_CESSIONARIO] as Cessionario
                                   ,[DT_CESSAO]		as DtCessao
                                   ,[DT_VENCIMENTO]  as DtVencimento
                                   ,[NM_BANDEIRA]	as Brand
                                   ,[NM_EMISSOR]		as Emissor
                                   ,[VL_CESSAO]		as Valor
                                   ,[CD_PROPOSTA]	as IdProposta
                             
                               FROM [BD_FINANCEIRO].[dbo].[TBSTONEF_DADOS_CESSAO] (NOLOCK)
                             
                             
                                 WHERE cast(DH_TIMESTAMP as date) = '2017-06-14'";

            List<DadosCessao> DadosCessao = dbWill.ExecuteReader<DadosCessao>(Query).ToList();

            return DadosCessao;


        }


        public static List<VSS600> RecebimentoVisa()
        {
            string dbTreasuryConnectionString = Global.ConnectionStringBDTreasury;

            DatabaseConnector dbTreasury = new DatabaseConnector(dbTreasuryConnectionString, 100000);

            string Query = @"    SELECT   [DT_BrandProcessing] AS DataLiquidacao
                                         ,[VL_SettlementAmount] AS Recebimento
                                         ,[DS_Company] as Empresa
                                 FROM [DBTREASURY].[AR].[Vss600] (nolock)

                                 where DT_Reference = '2017-06-14'
                                 and DS_Company = 'Elavon'";

            List<VSS600> RecebimentoVisa = dbTreasury.ExecuteReader<VSS600>(Query).ToList();

            return RecebimentoVisa;
        }





    }
}
