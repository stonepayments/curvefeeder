﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CurveFeeder;
using Newtonsoft.Json;
using RestSharp;
using CurvasDTO;
using System.Net.Http;


namespace CurveFeeder
{
    public class Requests
    {
        public static void GetToken(string username, string senha)
        {
            Autenticacao aut = new Autenticacao()
            {
                username = username,
                password = senha
            };

            var autjson = JsonConvert.SerializeObject(aut);

            HttpClient ClienteAutenticacao = new HttpClient();

            ClienteAutenticacao.Timeout = TimeSpan.FromSeconds(5);

            ClienteAutenticacao.DefaultRequestHeaders.Add("Accept", "application/json");
            
            var Response = ClienteAutenticacao.PostAsync(Global.BaseURLDebug + "authenticate/login", new StringContent(autjson, Encoding.UTF8, "application/json"));

            var Resultado = Response.Result.Content.ReadAsStringAsync().Result;

            Global.AuthToken = JsonConvert.DeserializeObject<Token>(Resultado).AuthToken;
            
            
        }





    }


}

