﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurvasDTO
{
    public class DadosCessao
    {
        public  string Cessionario { get; set; }

        public DateTime DtCessao { get; set; }

        public DateTime DtVencimento { get; set; }

        public string Brand { get; set; }

        public string Emissor { get; set; }

        public double Valor { get; set; }

        public int IdProposta { get; set; }


    }
}
