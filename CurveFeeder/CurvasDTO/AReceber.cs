﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurvasDTO
{
    public class AReceber
    {
        public DateTime Data { get; set; }

        public int Brand { get; set; }

        public int Produto { get; set; }

        public int TxType { get; set; }

        public double GrossAmount { get; set; }

        public double IC { get; set; }

    }
}
