﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurvasDTO
{
    public class VSS600
    {
        public DateTime DataLiquidacao { get; set; }

        public double Recebimento { get; set; }

        public string Empresa { get; set; }

    }
}
